public class Main {
  //Entry point of program
  public static void main(String[] args) {
    Game game = new Game();
    game.start();
  }
}
