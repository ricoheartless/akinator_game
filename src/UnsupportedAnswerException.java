public class UnsupportedAnswerException extends RuntimeException {
  UnsupportedAnswerException(String msg){
    super(msg);
  }
}
