import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import org.json.JSONArray;
import org.json.JSONObject;

public class JsonAnimalParser {

  public List<Animal> parse(String filepath) {
    final String database = parseFile(filepath);
    JSONObject object = new JSONObject(database);
    JSONObject temp;
    Animal animal;
    List<Animal> animals = new ArrayList<>();
    JSONArray array = object.getJSONArray("animals");
    for (int i = 0; i < array.length(); i++) {
      temp = new JSONObject(array.get(i).toString());
      animal = new Animal();
      animal.setName(temp.get("name").toString());
      animal.setUnique(temp.get("unique").toString());
      JSONArray tags = temp.getJSONArray("tags");
      List<String> resTags = new ArrayList<>();
      for (int j = 0; j < tags.length(); j++) {
        resTags.add(tags.get(j).toString());
      }
      animal.setTags(resTags);
      animals.add(animal);
    }
    return animals;
  }

  private String parseFile(String filepath) {
    StringBuilder stringBuilder = new StringBuilder();
    final String res;
    try {
      Scanner scanner = new Scanner(new File(filepath));
      while (scanner.hasNextLine()) {
        stringBuilder.append(scanner.nextLine());
      }
      scanner.close();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
    res = stringBuilder.toString();
    return res;
  }

}
