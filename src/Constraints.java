public class Constraints {

  public static final String filepath = "database_animals.json";

  public final static String SMALL = "small";
  public final static String BIG = "big";
  public final static String INSECT = "insects";
  public final static String PREDATOR = "predator";
  public final static String SAFARI = "safari";
  public final static String WATER = "water";
  public final static String BIRD = "bird";
  public final static String DOMESTIC = "domestic";
  public final static String PREY = "prey";

  public final static String BIG_QUESTION = "Тварина, яку ви загадали, великих розмірів*? *Більше людини.";
  public final static String INSECT_QUESTION = "Ця тварина комахоподібна?";
  public final static String PREDATOR_QUESTION = "Вона харчується іншими тваринами?";
  public final static String SAFARI_QUESTION = "Якщо поїхати на сафарі, її можна зустріти там?";
  public final static String WATER_QUESTION = "Чи водиться вона біля води?";
  public final static String BIRD_QUESTION = "Ця тварина літає?";
  public final static String DOMESTIC_QUESTION = "Це свійська тварина?";

  public final static String GREETING = "Вітаю в грі акінатор! Загадай любу тварину зі списку, а я її спробую відгадати!";
  public final static String FACE = ""
      + "               .__....._             _.....__,\n"
      + "                 .\": o :':         ;': o :\".\n"
      + "                 `. `-' .'.       .'. `-' .'\n"
      + "                   `---'             `---'\n"
      + "\n"
      + "         _...----...      ...   ...      ...----..._\n"
      + "      .-'__..-\"\"'----    `.  `\"`  .'    ----'\"\"-..__`-.\n"
      + "     '.-'   _.--\"\"\"'       `-._.-'       '\"\"\"--._   `-.`\n"
      + "     '  .-\"'                  :                  `\"-.  `\n"
      + "       '   `.              _.'\"'._              .'   `\n"
      + "             `.       ,.-'\"       \"'-.,       .'\n"
      + "               `.                           .'\n"
      + "                 `-._                   _.-'\n"
      + "                     `\"'--...___...--'\"`\n";
}
