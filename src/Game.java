import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;

public class Game {

  private List<Animal> animalList;
  private Map<String, String> tagQuestionMap;
  private List<String> tagList;
  private List<String> questionsList;
  private List<String> badList;
  private List<String> choiceList;

  private List<Animal> setAnimalList() {
    return new JsonAnimalParser().parse(Constraints.filepath);
  }

  private final Map<String, String> setMap() {
    Map<String, String> tagQuestionsMap = new HashMap<>();
    tagQuestionsMap.put(Constraints.BIG, Constraints.BIG_QUESTION);
    tagQuestionsMap.put(Constraints.INSECT, Constraints.INSECT_QUESTION);
    tagQuestionsMap.put(Constraints.PREDATOR, Constraints.PREDATOR_QUESTION);
    tagQuestionsMap.put(Constraints.SAFARI, Constraints.SAFARI_QUESTION);
    tagQuestionsMap.put(Constraints.WATER, Constraints.WATER_QUESTION);
    tagQuestionsMap.put(Constraints.DOMESTIC, Constraints.DOMESTIC_QUESTION);
    tagQuestionsMap.put(Constraints.BIRD, Constraints.BIRD_QUESTION);
    return tagQuestionsMap;
  }

  public void start() {
    System.out.println(Constraints.GREETING + "\n"+ Constraints.FACE + "Список: ");
    animalList = setAnimalList();
    tagQuestionMap = setMap();
    tagList = setTagList();
    choiceList = new ArrayList<>();
    animalList.stream().map(Animal::getName).forEach(System.out::println);
    for (String tag : tagList) {
      System.out.println(tagQuestionMap.get(tag));
      proceedUserDecision(tag, userInput());
    }
    if (getGuess().size() > 1) {
      for (Animal a : getGuess()) {
        System.out.println("Ваша тварина " + a.getUnique() + "?");
        String answer = userInput();
        if (!StringUtils.isBlank(answer) && StringUtils.containsAny(answer, 'Y', 'y', 'n', 'N')) {
          if (StringUtils.containsIgnoreCase(answer, "y")) {
            System.out.println("Ваша тварина: " + a.getName());
            break;
          }
        }
      }
    } else {
      System.out.println(Constraints.FACE + "Ваша тварина: " + getGuess().get(0).getName());
    }
  }

  private List<String> setTagList() {
    List<String> stringQueue = new LinkedList<>();
    stringQueue.addAll(tagQuestionMap.keySet());
    Collections.shuffle(stringQueue);
    return stringQueue;
  }

  private String userInput() {
    Scanner scanner = new Scanner(System.in);
    System.out.print("[y/n]:> ");
    return scanner.nextLine();
  }

  private void proceedUserDecision(String tag, String answer) {
    if (!StringUtils.isBlank(answer) && StringUtils.containsAny(answer, 'Y', 'y', 'n', 'N')) {
      if (StringUtils.containsIgnoreCase(answer, "y")) {
        manageDecision(tag, true);
      } else {
        manageDecision(tag, false);
      }
    } else {
      throw new UnsupportedAnswerException("Your answer is incorrect!");
    }
  }

  private List<Animal> getGuess() {
    List<Animal> res = new ArrayList<>();
    for (Animal a : animalList) {
      if (a.getTags().containsAll(choiceList) && a.getTags().size() == choiceList.size()) {
        res.add(a);
      }
    }
    return res;
  }

  private void manageDecision(String tag, boolean answer) {
    if (Constraints.BIG.equals(tag)) {
      if (answer) {
        choiceList.add(tag);
      } else {
        choiceList.add(Constraints.SMALL);
      }
    } else if (Constraints.PREDATOR.equals(tag)) {
      if (answer) {
        choiceList.add(tag);
      } else {
        choiceList.add(Constraints.PREY);
      }
    } else {
      if (answer) {
        choiceList.add(tag);
      }
    }
  }
}
